#pragma once

#include "main.h"

extern const size_t struct_map_size;
typedef struct map *Map_ref;
#define AMap_ref Map_ref __attribute__((cleanup(map_destroy)))

typedef struct {
    Rect camera;
    int w, h;
} Map_info;

void map_init(Map_ref, Rect camera, int w, int h);

inline __attribute__((always_inline))
Map_ref map_create(Rect camera, int w, int h)
{
    extern const size_t struct_tile_size;
    Map_ref new_map = alloca(struct_map_size + (struct_tile_size * w * h));
    map_init(new_map, camera, w, h);
    return new_map;
}

void map_destroy(Map_ref *);

void map_draw(Map_ref);
void map_move_cursor(Map_ref map, Point direction);
