#pragma once

#include "main.h"

extern const size_t fightscene_size;
typedef struct fightscene *FightScene;
#define AFightScene FightScene __attribute__((cleanup(fightscene_destroy)))

typedef struct {
    const char *bg_fname;
} FightSceneInfo;

void fightscene_init(FightScene, FightSceneInfo);

inline __attribute__((always_inline))
FightScene fightscene_create(FightSceneInfo info) {
    FightScene new_fightscene = alloca(fightscene_size);
    fightscene_init(new_fightscene, info);
    return new_fightscene;
}

void fightscene_destroy(FightScene *);

void fightscene_draw(FightScene);
