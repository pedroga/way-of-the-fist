#pragma once

#include "main.h"
#include "engine/sprite.h"

extern const size_t fighter_size;
typedef struct fighter *Fighter;
typedef Fighter *FighterRef;
#define AFighter Fighter __attribute__((cleanup(fighter_destroy)))

typedef struct {
    enum which_sprite sprite;
    int group;
    int hp;
    Point start_pos;
} FighterInfo;

void fighter_init(Fighter, Sprite, FighterInfo);

inline __attribute__((always_inline))
Fighter fighter_create(FighterInfo info)
{   log_calltrace();
    Fighter new_fighter = alloca(fighter_size);
    Sprite fighter_sprite = sprite_create(info.sprite);
    fighter_init(new_fighter, fighter_sprite, info);
    return new_fighter;
}

void fighter_destroy(FighterRef);

void fighter_update(Fighter);
void fighter_draw(Fighter);
void fighter_move(Fighter, Point direction);
void fighter_jump(Fighter);
void fighter_punch(Fighter);
void fighter_kick(Fighter);
void fighter_flip(Fighter);

bool fighter_is_dead(Fighter);

Rect fighter_get_pos(Fighter);
Rect fighter_get_closest_enemy(Fighter);
