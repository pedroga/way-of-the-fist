#include "main.h"
#include "engine/video.h"
#include "engine/physics.h"
#include "engine/loggers.h"

// Implements.
#include "common/fighter.h"

enum which_state {
    FS_STANDING, FS_PREPARING,
    FS_MOVING_LEFT, FS_MOVING_RIGHT,
    FS_JUMPING, FS_JUMPING_LEFT, FS_JUMPING_RIGHT,
    FS_FALLING, FS_FALLING_LEFT, FS_FALLING_RIGHT,
    FS_PUNCHING, FS_KICKING,
    FS_TAKING_DAMAGE
};

struct fighter {
    struct fighter_state {
        bool ended; // If the current state ended.
        enum which_state current;
        enum which_state next; // Queued state.
    } state;
    int box_id;
    int hp;
    Sprite sprite;
    Texture shadow_texture;
};

#define FIGHTER_SCALE 3

const size_t fighter_size = sizeof(struct fighter);

// Time in milliseconds that each action takes.
// TODO: adjust this table.
static const int durations[] = {
    [FS_STANDING] = 2000,
    [FS_PREPARING] = 1000,
    [FS_MOVING_LEFT] = 400,
    [FS_MOVING_RIGHT] = 400,
    [FS_JUMPING] = 1200,
    [FS_JUMPING_LEFT] = 1000,
    [FS_JUMPING_RIGHT] = 1000,
    [FS_FALLING] = 1200,
    [FS_FALLING_LEFT] = 1000,
    [FS_FALLING_RIGHT] = 1000,
    [FS_PUNCHING] = 1200,
    [FS_KICKING] = 2000,
    [FS_TAKING_DAMAGE] = 800,
};

// Mapping between fighter states and fighter animations.
static const Fal state_animation_map[] = {
    [FS_STANDING] = FAL_STANDING,
    [FS_PREPARING] = FAL_PREPARING,
    [FS_MOVING_LEFT] = FAL_WALKING,
    [FS_MOVING_RIGHT] = FAL_WALKING,
    [FS_JUMPING] = FAL_JUMPING,
    [FS_JUMPING_LEFT] = FAL_JUMPING,
    [FS_JUMPING_RIGHT] = FAL_JUMPING,
    [FS_FALLING] = FAL_JUMPING,
    [FS_FALLING_LEFT] = FAL_JUMPING,
    [FS_FALLING_RIGHT] = FAL_JUMPING,
    [FS_PUNCHING] = FAL_PUNCHING,
    [FS_KICKING] = FAL_KICKING,
    [FS_TAKING_DAMAGE] = FAL_TAKING_DAMAGE,
};

void fighter_init(Fighter fighter, Sprite sprite, FighterInfo info)
{   log_calltrace();
    (*fighter) = (struct fighter) {
        .state = (struct fighter_state) { .current = FS_PREPARING },
        .box_id = physics_create_cbox((CollisionBoxInfo) {
            .group = info.group,
            .rect = (Rect) { .x = info.start_pos.x }
        }),
        .hp = info.hp,
        .sprite = sprite,
        .shadow_texture = video_load_texture(FSHADOW_PATH),
    };
    sprite_start_animation(fighter->sprite, FAL_PREPARING, durations[FS_STANDING]);
}

void fighter_destroy(FighterRef fighter)
{   log_calltrace();
    sprite_destroy(&(*fighter)->sprite);
    *fighter = NULL;
}

#define JUMP_VECTOR (Point) {.x = 0, .y = -10}
#define JUMP_LEFT_VECTOR (Point) {.x = -5, .y = -15}
#define JUMP_RIGHT_VECTOR (Point) {.x = 5, .y = -15}
#define FALL_VECTOR (Point) {.x = 0, .y = 0}
#define FALL_LEFT_VECTOR (Point) {.x = -5, .y = 0}
#define FALL_RIGHT_VECTOR (Point) {.x = 5, .y = 0}
#define MOVE_LEFT_VECTOR (Point) {.x = -5, .y = 0}
#define MOVE_RIGHT_VECTOR (Point) {.x = 5, .y = 0}

void fighter_update(Fighter fighter)
{   log_calltrace();
    // Check if got hit.
    if (physics_cbox_is_hit(fighter->box_id)) {
        fighter->state.next = FS_TAKING_DAMAGE;
        fighter->state.ended = true;
        fighter->hp -= 20;
        // If died, remove cbox.
        if (fighter->hp < 0) {
            physics_cbox_clear(&fighter->box_id);
            return;
        }
    }
    // Allow to change state from standing before the animation ends.
    else if (fighter->state.current == FS_STANDING && fighter->state.next != FS_STANDING)
        fighter->state.ended = true;
    // If the fighter is attacking, check if it hit something.
    else if (!fighter->state.ended &&
             (fighter->state.current == FS_PUNCHING ||
              fighter->state.current == FS_KICKING)) {
        // TODO: get hit-box from animation and pass it to physics_cbox_hit().
        physics_cbox_hit(fighter->box_id);
    }
    // Don't change if not in the ground and if not jumping (basically, when falling).
    if (!(physics_grounded(fighter->box_id)) &&
        !(fighter->state.current == FS_JUMPING) &&
        !(fighter->state.current == FS_JUMPING_LEFT) &&
        !(fighter->state.current == FS_JUMPING_RIGHT))
        fighter->state.ended = false;
    if (fighter->state.ended) {
        {// Update current state.
            if (fighter->state.current == FS_JUMPING) {
                fighter->state.current = FS_FALLING;
                physics_cbox_add_vector(fighter->box_id, FALL_VECTOR);
            } else if (fighter->state.current == FS_JUMPING_LEFT) {
                fighter->state.current = FS_FALLING_LEFT;
                physics_cbox_add_vector(fighter->box_id, FALL_LEFT_VECTOR);
            } else if (fighter->state.current == FS_JUMPING_RIGHT) {
                fighter->state.current = FS_FALLING_RIGHT;
                physics_cbox_add_vector(fighter->box_id, FALL_RIGHT_VECTOR);
            } else if (fighter->state.next == FS_JUMPING) {
                switch (fighter->state.current) {
                case FS_MOVING_LEFT:
                    fighter->state.current = FS_JUMPING_LEFT;
                    physics_cbox_add_vector(fighter->box_id, JUMP_LEFT_VECTOR);
                    break;
                case FS_MOVING_RIGHT:
                    fighter->state.current = FS_JUMPING_RIGHT;
                    physics_cbox_add_vector(fighter->box_id, JUMP_RIGHT_VECTOR);
                    break;
                default:
                    fighter->state.current = FS_JUMPING;
                    physics_cbox_add_vector(fighter->box_id, JUMP_VECTOR);
                }
            } else if (fighter->state.next == FS_MOVING_LEFT) {
                fighter->state.current = FS_MOVING_LEFT;
                physics_cbox_add_vector(fighter->box_id, MOVE_LEFT_VECTOR);
            } else if (fighter->state.next == FS_MOVING_RIGHT) {
                fighter->state.current = FS_MOVING_RIGHT;
                physics_cbox_add_vector(fighter->box_id, MOVE_RIGHT_VECTOR);
            } else if (fighter->state.next == FS_PUNCHING ||
                       fighter->state.next == FS_KICKING) {
                fighter->state.current = fighter->state.next;
                physics_cbox_clear_vector(fighter->box_id);
            } else if (fighter->state.next == FS_TAKING_DAMAGE) {// Don't clear vector.
                fighter->state.current = fighter->state.next;
            } else {
                fighter->state.current = fighter->state.next;
                physics_cbox_clear_vector(fighter->box_id);
            }
            fighter->state.next = FS_STANDING;
        }
        {// Update animation.
            Fal next_animation = state_animation_map[fighter->state.current];
            sprite_start_animation(fighter->sprite, next_animation,
                                   durations[fighter->state.current]);
        }
    }
    {// Update current animation and
     // physics collision box according to new frame size.
        sprite_update_frame(fighter->sprite);
        Rect src_frame = sprite_cframe(fighter->sprite);
        int w = src_frame.w * FIGHTER_SCALE;
        int h = src_frame.h * FIGHTER_SCALE;
        physics_update_cbox_dim(fighter->box_id, w, h);
    }
}

static void schedule_state(Fighter fighter, enum which_state next)
{   log_calltrace();
    fighter->state.next = next;
}

void fighter_draw(Fighter fighter)
{   log_calltrace();
    Rect box = physics_get_cbox(fighter->box_id);
    Rect shadow_dst = (Rect) {.x = box.x, .w = box.w,
                              .y = box.y + box.h - (box.h / 14),
                              .h = box.h / 10};
    { // Adjust shadow color/position.
        if (!physics_grounded(fighter->box_id)) {
            // TODO: shadow_dst.y = physics_platform_below(fighter->box_id);
            shadow_dst.y = 700;
        }
        if (fighter->hp > 75) {
            SDL_SetTextureColorMod(fighter->shadow_texture, 8, 28, 0);
        } else if (fighter->hp > 50) {
            SDL_SetTextureColorMod(fighter->shadow_texture, 96, 96, 0);
        } else if (fighter->hp > 25) {
            SDL_SetTextureColorMod(fighter->shadow_texture, 128, 64, 0);
        } else {
            SDL_SetTextureColorMod(fighter->shadow_texture, 128, 0, 21);
        }
    }
    video_draw_texture(fighter->shadow_texture, NULL, &shadow_dst);
    fighter->state.ended = !sprite_draw(fighter->sprite, box);
}

void fighter_move(Fighter fighter, Point direction)
{   log_calltrace();
    assert(direction.y == 0 && direction.x != 0);
    if (direction.x > 0)
        schedule_state(fighter, FS_MOVING_RIGHT);
    else if (direction.x < 0)
        schedule_state(fighter, FS_MOVING_LEFT);
}

void fighter_jump(Fighter fighter)
{   log_calltrace();
    schedule_state(fighter, FS_JUMPING);
}

void fighter_punch(Fighter fighter)
{   log_calltrace();
    schedule_state(fighter, FS_PUNCHING);
}

void fighter_kick(Fighter fighter)
{   log_calltrace();
    schedule_state(fighter, FS_KICKING);
}

void fighter_flip(Fighter fighter)
{   log_calltrace();
    // TODO: add animation and delay for flipping:
    // schedule_state(fighter, FS_TURNING);
    if (fighter->state.current == FS_STANDING ||
        fighter->state.current == FS_MOVING_LEFT ||
        fighter->state.current == FS_MOVING_RIGHT)
        physics_flip_cbox(fighter->box_id);
}

bool fighter_is_dead(Fighter fighter)
{   log_calltrace();
    return fighter->hp < 0;
}


Rect fighter_get_pos(Fighter fighter)
{   log_calltrace();
    Rect cbox = physics_get_cbox(fighter->box_id);
    return cbox;
}

Rect fighter_get_closest_enemy(Fighter fighter)
{   log_calltrace();
    return physics_get_closest_enemy(fighter->box_id);
}
