#include "engine/video.h"
#include "engine/loggers.h"

// Implements.
#include "common/map.h"

/*****************************************************
 *                    DEFINITIONS                    *
 *****************************************************/

#define TILE_SIZE 64

typedef enum {
    TK_VOID, TK_DESERT, TK_GRASS, TK_TOWN, TK_DUNGEON, TK_WATER,
    TK_AMOUNT} Tile_kind;

typedef struct tile *Tile_ref;
typedef struct tile {
    Tile_kind kind;
} Tile;
const size_t struct_tile_size = sizeof(struct tile);

typedef struct map {
    Rect camera; // The portion of the map that is drawn.
    int w, h; // The map dimensions.
    Point player_pos, cursor;
    Texture player_texture;
    Texture tile_textures[TK_AMOUNT];
    Tile tiles[];
} Map;
const size_t struct_map_size = sizeof(struct map);


/***************************************************
 *                    FUNCTIONS                    *
 ***************************************************/

// Positions from map (matrix (row, col) to array (idx)).
Tile_ref tile_at(Map_ref map, int row, int col)
{   log_calltrace();
    return map->tiles + col + row * map->w;
}

void map_init(Map_ref map, Rect camera, int w, int h)
{   log_calltrace();
    {// Load textures.
        map->player_texture = video_load_texture(PIXEL_MAN_0);
        map->tile_textures[TK_VOID] = NULL;
        map->tile_textures[TK_DESERT] = video_load_texture(TILE_DESERT);
        map->tile_textures[TK_GRASS] = video_load_texture(TILE_GRASS);
        map->tile_textures[TK_DUNGEON] = video_load_texture(TILE_DUNGEON);
    }
    map->player_pos = map->cursor = (Point) {.x = 5, .y = 5};
    map->camera = camera;
    map->w = w; map->h = h;
    for (int i = 0; i < w * h; ++i) {
        map->tiles[i].kind = TK_VOID;
    }
    // TODO: this should be procedurally generated.
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < w; ++j)
            tile_at(map, i, j)->kind = TK_DESERT;
    }
    for (int i = 5; i < h; ++i) {
        for (int j = 0; j < w; ++j)
            tile_at(map, i, j)->kind = TK_GRASS;
    }
    tile_at(map, 1, 2)->kind = TK_DUNGEON;
}

void map_destroy(Map_ref *map)
{   log_calltrace(); assert(map && *map);
    video_destroy_texture(&(*map)->player_texture);
    for (int i = 1; i < TK_AMOUNT; ++i)
        video_destroy_texture(&(*map)->tile_textures[i]);
    *map = NULL;
}

void map_draw(Map_ref map)
{   log_calltrace();
    Rect dst = (Rect) {0, 0, TILE_SIZE, TILE_SIZE};
    video_draw_rect(&map->camera, COLOR_WHITE);
    for (int i = 0; i < map->h && dst.y < map->camera.h; ++i) {
        for (int j = 0; j < map->w && dst.x < map->camera.w; ++j) {
            Tile_kind tk = tile_at(map, i, j)->kind;
            if (tk != TK_VOID) {
                Rect src = {0, 0, dst.w, dst.h};
                video_draw_texture(map->tile_textures[tk], &src, &dst);
            } else {
                video_draw_rect(&dst, COLOR_WHITE);
            }
            dst.x += TILE_SIZE;
        }
        dst.x = 0; dst.y += TILE_SIZE;
    }
    {// Draw cursor.
        dst.x = map->cursor.x * TILE_SIZE;
        dst.y = map->cursor.y * TILE_SIZE;
        for (int i = 0; i < 3; ++i) {
            video_draw_rect(&dst, COLOR_CURSOR);
            ++dst.x; ++ dst.y; dst.w -= 2; dst.h -= 2;
        }
    }
    {// Draw player.
        dst.x = map->player_pos.x * TILE_SIZE;
        dst.y = map->player_pos.y * TILE_SIZE - (TILE_SIZE / 3);
        Rect src = (Rect) {0, 0, 32, 32};
        printf("DRAW_PLAYER at (%d, %d)\n", dst.x, dst.y);
        video_draw_texture(map->player_texture, &src, &dst);
    }
}

void map_move_cursor(Map_ref map, Point direction)
{   log_calltrace();
    map->cursor.x += direction.x;
    map->cursor.y += direction.y;
}
