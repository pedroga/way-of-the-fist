#include "main.h"
#include "engine/loggers.h"

// Implements.
#include "common/widgets/button.h"

struct button {
    Rect src;
    bool selected;
    Texture sel_texture, notsel_texture;
    Texture label_texture;
};

const size_t button_size = sizeof(struct button);

void button_init(Button btn, ButtonInfo btn_info)
{   log_calltrace();
    assert(btn);
    *btn = (struct button) {
        .src = btn_info.rect,
        .selected = false,
        .notsel_texture = video_load_texture(BTN_NOT_SELECTED),
        .sel_texture = video_load_texture(BTN_SELECTED),
        .label_texture = video_load_texture_from_text(btn_info.label, COLOR_BLACK)
    };
}

void button_destroy(ButtonRef btn)
{   log_calltrace();
    video_destroy_texture(&(*btn)->sel_texture);
    video_destroy_texture(&(*btn)->notsel_texture);
    video_destroy_texture(&(*btn)->label_texture);
    *btn = NULL;
}

void button_draw(Button btn, Point _dst)
{   log_calltrace();
    Rect dst = {.x = _dst.x, .y = _dst.y,
                .w = btn->src.w, .h = btn->src.h};
    video_draw_texture((btn->selected) ? btn->sel_texture : btn->notsel_texture,
                       NULL, &dst);
    int label_offset = 13;
    dst.x += label_offset;
    dst.w -= label_offset * 2;
    video_draw_texture(btn->label_texture, NULL, &dst);
}

void button_select(Button btn)
{
    btn->selected = true;
}

void button_unselect(Button btn)
{
    btn->selected = false;
}
