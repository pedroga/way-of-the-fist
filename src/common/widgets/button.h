#pragma once

#include "main.h"
#include "engine/video.h"

extern const size_t button_size;

typedef struct button *Button;
typedef Button *ButtonRef;
#define AButton Button __attribute__((cleanup(button_destroy)))

typedef struct {
    Rect rect;
    char *label;
} ButtonInfo;

/*
#define button_create(ButtonInfo btn_info) ({   \
    Button new_button = alloca(button_size);    \
})
*/

void button_init(Button btn, ButtonInfo btn_info);

inline __attribute__((always_inline))
Button button_create(ButtonInfo btn_info) {
    Button new_button = alloca(button_size);
    button_init(new_button, btn_info);
    return new_button;
}

void button_destroy(ButtonRef btn);

void button_draw(Button btn, Point dst);

void button_select(Button btn);
void button_unselect(Button btn);
