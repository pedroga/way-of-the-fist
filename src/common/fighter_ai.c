#include "engine/loggers.h"
#include "common/fighter.h"

// Implements.
#include "common/fighter_ai.h"

/* Should not modify enemy. */
void fighter_ai(Fighter fighter)
{   log_calltrace();
    Rect pos = fighter_get_pos(fighter);
    Rect target = fighter_get_closest_enemy(fighter);

    // TODO: compare dist to fighter width of attacking animation.
    if (rect_dist(pos, target) < 200) {
        int r = rand() % 100;
        if (r < 50)
            fighter_kick(fighter);
        else
            fighter_punch(fighter);
    } else {
        int rjump = rand() % 100;
        if (rjump < 2)
            fighter_jump(fighter);
        else {
            if (pos.x < target.x) {
                if (pos.w < 0)
                    fighter_flip(fighter);
                fighter_move(fighter, DEAST);
            } else if (pos.x > target.x) {
                if (pos.w > 0)
                    fighter_flip(fighter);
                fighter_move(fighter, DWEST);
            }
        }
    }
}
