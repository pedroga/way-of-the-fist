#include "engine/video.h"
#include "engine/loggers.h"

// Implements.
#include "common/fight_scene.h"

struct fightscene {
    Texture bg_texture;
};

const size_t fightscene_size = sizeof(struct fightscene);

void fightscene_init(FightScene scene, FightSceneInfo info)
{   log_calltrace();
    scene->bg_texture = video_load_texture(info.bg_fname);
}

void fightscene_destroy(FightScene *scene)
{   log_calltrace();
    video_destroy_texture(&(*scene)->bg_texture);
}

void fightscene_draw(FightScene scene)
{   log_calltrace();
    video_draw_texture(scene->bg_texture, NULL, NULL);
}
