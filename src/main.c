#include "engine/video.h"
#include "engine/input.h"
#include "engine/loggers.h"
#include "modes/modes.h"

int main(int argc, char **argv)
{   log_calltrace();
    srand(time(NULL));
    video_init();
    input_init();
    title_mode();
    //fight_mode();

    return 0;
}
