#include "engine/video.h"
#include "engine/input.h"
#include "engine/loggers.h"
#include "engine/assets.h"
#include "common/map.h"

// Implements.
#include "modes/modes.h"

void overworld_mode(void)
{   log_calltrace();
    // Global data.
    Map_ref map;

    {// Mode init.
        // TODO: load map.
        map = map_create((Rect) {0, 0, 1360, 768}, 50, 50);
    }
    { // Mode meat.
        bool loop = true;

        void redraw(void)
        {   log_calltrace();
            video_clear();
            map_draw(map);
            video_present();
        }

        void update(void)
        {   log_calltrace();

        }

        void handle_input(void)
        {   log_calltrace();
            input_get();
            if (g_input.esc) {
                loop = false;
            }
            if (g_input.up) {
                map_move_cursor(map, DNORTH);
            }
            if (g_input.down) {
                map_move_cursor(map, DSOUTH);
            }
            if (g_input.left) {
                map_move_cursor(map, DWEST);
            }
            if (g_input.right) {
                map_move_cursor(map, DEAST);
            }
        }

        while (loop) {
            // TODO: handle_input() should have a delay instead of calling input_reset() here.
            input_reset();
            handle_input();
            update();
            redraw();
        }
    }
    { // Mode deinit.

    }
}
