#include "main.h"
#include "engine/video.h"
#include "engine/input.h"
#include "engine/loggers.h"
#include "engine/physics.h"
#include "engine/assets.h"
#include "common/fight_scene.h"
#include "common/fighter.h"
#include "common/fighter_ai.h"

// Implements.
#include "modes/modes.h"

void fight_mode(void)
{   log_calltrace();
    // Global data.
    AFightScene scene;
    AFighter player;
    //AFighter enemy;

    {// Mode init.
        physics_push(true);
        scene = fightscene_create((FightSceneInfo) {
            .bg_fname = FIGHT_SCENE_1
        });
        player = fighter_create((FighterInfo) {
            .sprite = SPRITE_OMEGA,
            .group = GROUP_PLAYER,
            .hp = 100,
            // TODO: get start_pos from scene.
            .start_pos = (Point) {.x = 0}
        });
/*
        enemy = fighter_create((FighterInfo) {
            .sprite = SPRITE_SAGAT,
            .group = GROUP_ENEMY,
            .hp = 100,
            .start_pos = (Point) {.x = 800}
        });
*/
    }
    {// Mode meat.
        bool loop = true;
        void redraw(void)
        {   log_calltrace();
            video_clear();
            fightscene_draw(scene);
            if (!fighter_is_dead(player))
                fighter_draw(player);
/*
            if (!fighter_is_dead(enemy))
                fighter_draw(enemy);
*/
            video_present();
        }

        void update(void)
        {   log_calltrace();
            //fighter_ai(enemy);
            if (fighter_is_dead(player)) {
                gameover_mode();
                loop = false;
                return;
            } else {
                fighter_update(player);
            }
/*
            if (!fighter_is_dead(enemy))
                fighter_update(enemy);
*/
            // physics must always be updated last.
            physics_update();
        }

        void handle_input(void)
        {   log_calltrace();
            input_get();
            if (g_input.esc) {
                loop = false;
            }
            if (g_input.left) {
                fighter_move(player, DWEST);
            }
            if (g_input.right) {
                fighter_move(player, DEAST);
            }
            if (g_input.up) {
                fighter_jump(player);
            }
            if (g_input.confirm) {
                fighter_punch(player);
            }
            if (g_input.cancel) {
                fighter_kick(player);
            }
            if (g_input.turn) {
                fighter_flip(player);
            }
        }

        while (loop) {
            // TODO: handle_input() should have a delay instead of calling input_reset() here.
            input_reset();
            handle_input();
            update();
            redraw();
        }
    }
    {// Mode deinit.
        physics_pop();
    }
}
