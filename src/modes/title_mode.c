#include "engine/video.h"
#include "engine/input.h"
#include "engine/loggers.h"

// Implements.
#include "modes/modes.h"

void title_mode(void)
{   log_calltrace();

    enum which_button {
        BTN_NEWGAME, BTN_CONTINUE, BTN_OPTIONS, BTN_EXIT, BTN_CREDITS,
        BTN_AMOUNT};

    // Global data.
    bool loop = true;
    ATexture bg_texture = video_load_texture(TITLE_IMAGE);
    ATexture btns_texture = video_load_texture(TITLE_BUTTONS);
    Rect title_btns[] = {
        [BTN_NEWGAME] = (Rect) {.x = 260, .y = 120, .w = 160, .h = 101},
        [BTN_CONTINUE] = (Rect) {.x = 217, .y = 267, .w = 258, .h = 117},
        [BTN_OPTIONS] = (Rect) {.x = 241, .y = 390, .w = 281, .h = 104},
        [BTN_CREDITS] = (Rect) {.x = 506, .y = 165, .w = 124, .h = 82},
        [BTN_EXIT] = (Rect) {.x = 285, .y = 513, .w = 181, .h = 100},
    };
    int selected_btn = BTN_CONTINUE;
    { // Mode init.
        input_reset();
        video_clear();
    }
    { // Mode meat.
        void redraw(void)
        {   log_calltrace();
            video_clear();
            video_draw_texture(bg_texture, NULL, NULL);
            Rect *sbtn_rect = title_btns + selected_btn;
            video_draw_texture(btns_texture, sbtn_rect, sbtn_rect);
            video_present();
        }

        void handle_input(void)
        {   log_calltrace();
            input_get();
            if (g_input.up) {
                if (selected_btn == 0) selected_btn = BTN_AMOUNT - 1;
                else --selected_btn;
            }
            if (g_input.down) {
                if (selected_btn == BTN_AMOUNT - 1) selected_btn = 0;
                else ++selected_btn;
            }
            if (g_input.esc) {
                loop = false;
            }
            if (g_input.confirm) {
                switch (selected_btn) {
                case BTN_NEWGAME:
                    // TODO: create character.
                    break;
                case BTN_CONTINUE:
                    // TODO: load game.
                    //overworld_mode();
                    fight_mode();
                    break;
                case BTN_OPTIONS:
                    // TODO: options_mode.
                    break;
                case BTN_CREDITS:
                    // TODO: credits_mode.
                    break;
                case BTN_EXIT:
                    loop = false;
                    break;
                }
            }
            input_reset();
        }

        while (loop) {
            handle_input();
            redraw();
        };
    }
    { // Mode deinit.
    }
}
