#include "engine/video.h"
#include "engine/input.h"
#include "engine/loggers.h"

// Implements.
#include "modes/modes.h"

void gameover_mode(void)
{   log_calltrace();

    // Global data.
    bool loop = true;
    ATexture bg_texture = video_load_texture(GAMEOVER);
    { // Mode init.
        input_reset();
    }
    { // Mode meat.
        void redraw(void)
        {   log_calltrace();
            video_clear();
            video_draw_texture(bg_texture, NULL, NULL);
            video_present();
        }

        void handle_input(void)
        {   log_calltrace();
            input_get();
            if (g_input.esc) {
                loop = false;
            }
            input_reset();
        }

        while (loop) {
            handle_input();
            redraw();
        };
    }
    { // Mode deinit.
    }
}
