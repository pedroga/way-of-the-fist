#pragma once

#include "main.h"

#ifdef DEBUG
struct _logger {
    const char * const func_name;
};
inline __attribute__((always_inline))
void _enter(struct _logger *logger)
{
    fprintf(stdout, "enter: %s\n", logger->func_name);
}
inline __attribute__((always_inline))
void _leave(struct _logger *logger)
{
    fprintf(stdout, "leave: %s\n", logger->func_name);
}
#define log_calltrace()                                 \
    __attribute__((cleanup(_leave))) struct _logger     \
    _log = {.func_name = __func__};                     \
    _enter(&_log);
#else
#define log_calltrace() {}
#endif

#ifdef DEBUG
#define log_fps_start()                         \
    unsigned _fps_start = SDL_GetTicks();       \
    int _frames_rendered = 0;
#define log_fps()                                                       \
    unsigned _elapsed_time = (SDL_GetTicks() - _fps_start) / 1000;      \
    if (_elapsed_time > 0)                                              \
        printf("[FPS] %d (%d frames rendered)\n",                       \
               _frames_rendered / _elapsed_time,                        \
               _frames_rendered);                                       \
    ++_frames_rendered;
#else
#define log_fps_start() {}
#define log_fps() {}
#endif
