#pragma once

// Paths.
#define ASSETS_PATH "assets/"
#define FONTS_PATH ASSETS_PATH"fonts/"
#define TEXTURES_PATH ASSETS_PATH"textures/"
#define OVERWORLD_CHARACTERS_PATH TEXTURES_PATH"overworld-characters/"
#define FIGHTER_CHARACTERS_PATH TEXTURES_PATH"fighter-characters/"
#define FIGHT_SCENES_PATH TEXTURES_PATH"fight-scenes/"
#define TILES_PATH TEXTURES_PATH"tiles/"
#define MISC_PATH TEXTURES_PATH"misc/"

// Fonts.
#define DEFAULT_FONT FONTS_PATH"default-font.ttf"

// Buttons.
#define BTN_NOT_SELECTED TEXTURES_PATH"wotf-not-selected-button.png"
#define BTN_SELECTED TEXTURES_PATH"wotf-selected-button.png"

// Overworld characters.
#define PIXEL_MAN_0 OVERWORLD_CHARACTERS_PATH"pipoya-man-0.png"

// Fighter characters.
#define OMEGA_PATH FIGHTER_CHARACTERS_PATH"wotf-omega.png"
#define RYU_PATH FIGHTER_CHARACTERS_PATH"alpha3-ryu.png"
#define SAGAT_PATH FIGHTER_CHARACTERS_PATH"alpha3-sagat.png"
#define CAMMY_PATH FIGHTER_CHARACTERS_PATH"alpha3-cammy.png"

// Fight scenes.
#define FIGHT_SCENE_1 FIGHT_SCENES_PATH"fight_scene_vega.png"

// Tiles.
#define TILE_DESERT TILES_PATH"wotf-tile-desert.png"
#define TILE_GRASS TILES_PATH"wotf-tile-grass.png"
#define TILE_DUNGEON TILES_PATH"wotf-tile-dungeon.png"

// Others.
#define TITLE_IMAGE TEXTURES_PATH"wotf-title.png"
#define TITLE_BUTTONS TEXTURES_PATH"wotf-title-buttons.png"
#define GAMEOVER TEXTURES_PATH"gameover.jpg"
#define FSHADOW_PATH MISC_PATH"shadow.png"
