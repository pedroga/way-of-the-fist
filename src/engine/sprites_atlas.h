#pragma once

#include "engine/video.h"

#define MAX_FRAMES 21 // For each animation line.

enum which_sprite {
    SPRITE_OMEGA,
    SPRITE_RYU,
    SPRITE_SAGAT,
    //SPRITE_CAMMY,
    SPRITE_AMOUNT};

typedef enum {
    FAL_PREPARING,
    FAL_STANDING,
    FAL_WALKING,
    FAL_JUMPING,
    FAL_PUNCHING,
    FAL_KICKING,
    FAL_TAKING_DAMAGE,
    FAL_AMOUNT} Fal;

extern const Rect sprites_atlas[SPRITE_AMOUNT][FAL_AMOUNT][MAX_FRAMES];
