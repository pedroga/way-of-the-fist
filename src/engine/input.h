#pragma once

struct input {
    unsigned esc: 1; // Esc on keyboard; back on controller.
    unsigned up, down, left, right: 1;
    unsigned confirm: 1; // Enter on keyboard; A on controller.
    unsigned cancel: 1; // Backspace on keyboard; B on controller.
    unsigned turn: 1; // Tab on keyboard; L1/LB on controller.
};

extern struct input g_input;

void input_init(void);
void input_reset(void);
void input_get(void);

void input_connect_controller(void);
void input_disconnect_controller(void);
