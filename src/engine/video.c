#include "main.h"
#include "engine/loggers.h"

// Implements.
#include "engine/video.h"

/*
struct texture {
    SDL_Texture texture_ptr;
    int w, h;
};
*/

const Rect video_res = {
    .x = 0, .y = 0,
    .w = 1366, .h = 768
};

static SDL_DisplayMode display_mode;

static SDL_Window *g_window = NULL;
static SDL_Renderer *g_renderer = NULL;
static TTF_Font *g_font = NULL;
static SDL_Texture *g_texture = NULL;

static void video_deinit(void)
{   log_calltrace();
    TTF_CloseFont(g_font);
    SDL_DestroyRenderer(g_renderer);
    SDL_DestroyWindow(g_window);
    SDL_DestroyTexture(g_texture);
    SDL_Quit();
}

void video_init(void)
{   log_calltrace(); atexit(video_deinit);
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
	fprintf(stderr, "[ERROR] SDL_Init:  %s\n", SDL_GetError());
        exit(1);
    }
    if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG)) {
	fprintf(stderr, "[ERROR] IMG_Init: %s\n", IMG_GetError());
        exit(1);
    }
    if (TTF_Init() == -1) {
        fprintf(stderr, "[ERROR] TTF_Init: %s\n", TTF_GetError());
        exit(1);
    }
    if (SDL_GetDesktopDisplayMode(0, &display_mode) < 0) {
        fprintf(stderr, "[ERROR] SDL_GetDesktopDisplayMode: %s\n", SDL_GetError());
        exit(1);
    }
    if (!(g_window = SDL_CreateWindow("Way Of The Fist", 0, 0,
                                      display_mode.w, display_mode.h,
                                      SDL_WINDOW_FULLSCREEN))) {
        fprintf(stderr, "[ERROR] SDL_CreateWindow: %s\n", SDL_GetError());
        exit(1);
    }
    if (!(g_renderer = SDL_CreateRenderer(g_window, -1,
                                          SDL_RENDERER_ACCELERATED |
                                          SDL_RENDERER_PRESENTVSYNC |
                                          SDL_RENDERER_TARGETTEXTURE))) {
        fprintf(stderr, "[ERROR] SDL_CreateRenderer: %s\n", SDL_GetError());
        exit(1);
    }
    if (!(g_texture = SDL_CreateTexture(g_renderer, display_mode.format,
                                        SDL_TEXTUREACCESS_TARGET,
                                        video_res.w, video_res.h))) {
        fprintf(stderr, "[ERROR] SDL_CreateTexture: %s\n", SDL_GetError());
        exit(1);
    }
    if (!(g_font = TTF_OpenFont(DEFAULT_FONT, 64))) {
        fprintf(stderr, "[ERROR] TTF_OpenFont: %s\n", TTF_GetError());
        exit(1);
    }
}

void video_clear(void)
{   log_calltrace();
    SDL_SetRenderTarget(g_renderer, g_texture);
    SDL_SetRenderDrawColor(g_renderer, 0, 0, 0, 255);
    SDL_RenderClear(g_renderer);
}

void video_present(void)
{   log_calltrace();
    SDL_SetRenderTarget(g_renderer, NULL);
    SDL_RenderCopy(g_renderer, g_texture, NULL, NULL);
    SDL_RenderPresent(g_renderer);
}

void video_draw_rect(Rect *dst, Color c)
{   log_calltrace();
    assert(dst && dst->w > 0 && dst->h > 0);
    SDL_SetRenderDrawColor(g_renderer, c.r, c.g, c.b, c.a);
    SDL_RenderDrawRect(g_renderer, dst);
}

void video_draw_texture(Texture texture, SDL_Rect *src, SDL_Rect *dst)
{   log_calltrace();
    SDL_RendererFlip flip = SDL_FLIP_NONE;
    if (dst && dst->w < 0) { // Negative dst->w means that it should be flipped.
        flip = SDL_FLIP_HORIZONTAL;
        dst->w = -dst->w;
    }
    if (SDL_RenderCopyEx(g_renderer, texture, src, dst, 0, NULL, flip) < 0) {
        fprintf(stderr, "[ERROR] SDL_RenderCopy: %s\n", SDL_GetError());
        exit(1);
    }
}

Texture video_load_texture(const char *fname)
{   log_calltrace();
    SDL_Surface *surface = IMG_Load(fname);
    if (!surface) {
	fprintf(stderr, "[ERROR] IMG_Load: %s\n", IMG_GetError());
        exit(1);
    }
    Texture texture = SDL_CreateTextureFromSurface(g_renderer, surface);
    if (!texture) {
        fprintf(stderr, "[ERROR] SDL_CreateTextureFromSurface: %s\n", SDL_GetError());
        exit(1);
    }
    SDL_FreeSurface(surface);
    return texture;
}

Texture video_load_texture_from_text(const char *text, SDL_Color color)
{   log_calltrace();
    SDL_Surface *surface = TTF_RenderText_Solid(g_font, text, color);
    if (!surface) {
        fprintf(stderr, "[ERROR] TTF_RenderText_Solid: %s\n", TTF_GetError());
        exit(1);
    }
    Texture texture = SDL_CreateTextureFromSurface(g_renderer, surface);
    if (!texture) {
        fprintf(stderr, "[ERROR] SDL_CreateTextureFromSurface: %s\n", SDL_GetError());
        exit(1);
    }
    SDL_FreeSurface(surface);
    return texture;
}

void video_destroy_texture(Texture *texture)
{   log_calltrace();
    SDL_DestroyTexture(*texture);
    *texture = NULL;
}

bool rect_equals(Rect a, Rect b)
{   log_calltrace();
    return (
        a.x == b.x && a.y == b.y &&
        a.w == b.w && a.h == b.h
    );
}

int rect_dist(Rect a, Rect b)
{   log_calltrace();
    int dist = -1;

    // TODO: consider rects w/h.
    dist = point_dist((Point) {a.x, a.y}, (Point) {b.x, b.y});

    assert(dist >= 0);
    return dist;
}

bool point_equals(Point a, Point b)
{   log_calltrace();
    return a.x == b.x && a.y == b.y;
}

int point_dist(Point a, Point b)
{   log_calltrace();
    int dist = -1;

    dist = (int) sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2));

    assert(dist >= 0);
    return dist;
}
