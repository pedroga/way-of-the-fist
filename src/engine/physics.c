#include "main.h"
#include "engine/loggers.h"
#include "engine/video.h"

// Implements.
#include "engine/physics.h"

#define CONTEXT_MAX 3 // Maximal amount of stacked contexts.
#define CBOX_MAX 128 //
#define FLOOR_LEVEL 720 // Floor y value.

static int acontext = -1; // Active context.

typedef struct {
    bool is_alive;
    bool is_flipped;
    bool got_hit; // TODO: damage amount.
    unsigned last_hit; // Last time the cbox got hit.
    int group; // Boxes of the same group can overlap.
    Rect rect; // The position/dimensions of the box.
    Point fvector; // Active force vector.
} CollisionBox;

static struct {
    Point gravity;
    int cboxes_amount;
    CollisionBox cboxes[CBOX_MAX];
    // When getting a new cbox, its idx will be next_idx_queue[cboxes_amount].
    int next_idx_queue[CBOX_MAX];
} contexts[CONTEXT_MAX];

#define ccontext contexts[acontext] // Current context.
#define cboxes_at(box_id) ccontext.cboxes[(box_id)]
#define cbox_got_hit(box_id) SDL_GetTicks() - cboxes_at((box_id)).last_hit < DAMAGE_DELAY

#define DAMAGE_DELAY 2000

void physics_push(bool gravity)
{   log_calltrace();
    assert(acontext < CONTEXT_MAX - 1);
    ++acontext;
    if (gravity) contexts[acontext].gravity = (Point) {.x = 0, .y = 4};
    else contexts[acontext].gravity = (Point) {.x = 0, .y = 0};
    contexts[acontext].cboxes_amount = 0;
    for (int i = 0; i < CBOX_MAX; ++i) {
        ccontext.next_idx_queue[i] = i;
        ccontext.cboxes[i].is_alive = false;
    }
}

void physics_pop(void)
{   log_calltrace();
    assert(acontext >= 0);
    --acontext;
}

static bool rect_overlap(Rect a, Rect b)
{   log_calltrace();
    Point tla = (Point) {.x = a.x, .y = a.y}; // Top-left a.
    Point bra = (Point) {.x = a.x + a.w, .y = a.y + a.h}; // Bottom-right a.
    Point tlb = (Point) {.x = b.x, .y = b.y}; // Top-left b.
    Point brb = (Point) {.x = b.x + b.w, .y = b.y + b.h}; // Bottom-right b.
    return (bra.y >= tlb.y && bra.x >= tlb.x &&
            tla.y <= brb.y && tla.x <= brb.x);
}

static Point check_collision(Rect a, Rect b)
{   log_calltrace();
    Point result = (Point) {};
    Point tla = (Point) {.x = a.x, .y = a.y}; // Top-left a.
    Point bra = (Point) {.x = a.x + a.w, .y = a.y + a.h}; // Bottom-right a.
    Point tlb = (Point) {.x = b.x, .y = b.y}; // Top-left b.
    Point brb = (Point) {.x = b.x + b.w, .y = b.y + b.h}; // Bottom-right b.

    // TODO: some better logic may reduce the checks.
    // Check collision to the right.
    if ((tla.x < tlb.x && tlb.x < bra.x) &&
        (tlb.y < bra.y && brb.y > tla.y) &&
        (brb.x > bra.x)) {
        result.x = tlb.x - bra.x;
    }
    // Check collision to the left.
    if ((tla.x < brb.x && brb.x < bra.x) &&
        (tlb.y < bra.y && brb.y > tla.y) &&
        (brb.x < bra.x)) {
        result.x = brb.x - tla.x;
    }
    return result;
}

void physics_update(void)
{   log_calltrace();
    Rect new_rects[ccontext.cboxes_amount];
    {// Calculate new_pos of every collision box.
        for (int i = 0; i < ccontext.cboxes_amount; ++i) {
            CollisionBox cbox = cboxes_at(i);
            Rect new_pos = cbox.rect;
            // Apply the force vectors active on the cbox + gravity.
            // After got hit, small delay in movement.
            if (SDL_GetTicks() - cboxes_at(i).last_hit > DAMAGE_DELAY / 2) {
                new_pos.x += cbox.fvector.x + ccontext.gravity.x;
                new_pos.y += cbox.fvector.y + ccontext.gravity.y;
            }
            { // Don't go offscreen.
                if (new_pos.y + new_pos.h > FLOOR_LEVEL) {
                    new_pos.y = FLOOR_LEVEL - new_pos.h;
                } /*  TODO: don't go beyond the top of the screen.
                      else if (new_pos.y < 0) {
                      new_pos.y = 0;
                      }
                  */
                if (new_pos.x < 0) {
                    new_pos.x = 0;
                } else if (new_pos.x + new_pos.w > video_res.w) {
                    new_pos.x = video_res.w - new_pos.w;
                }
            }
            new_rects[i] = new_pos;
        }
    }
    {// Solve collisions.
        for (int i = 0; i < ccontext.cboxes_amount; ++i) {
            if (cbox_got_hit(i)) {
                continue; // If got hit, don't move no one.
            }
            Rect ri = new_rects[i];
            int mi = abs(cboxes_at(i).fvector.x);
            for (int j = 0; j < ccontext.cboxes_amount; ++j) {
                Rect rj = new_rects[j];
                int mj = abs(cboxes_at(j).fvector.x);
                Point collision;
                // If the cbox got hit, it doesn't cause collision.
                if (i != j &&
                    !point_equals((collision = check_collision(ri, rj)), (Point) {})) {
                    // If i has more movement force than j, push j aside.
                    if (mi > mj) {
                        // TODO: adjust amount to mi - mj.
                        rj.x -= collision.x;
                    }
                    // Cannot push someone past the edge of the screen.
                    if (rj.x < 0) {
                        rj.x = 0;
                        ri.x = rj.w;
                    } else if (rj.x + rj.w > video_res.w) {
                        rj.x = video_res.w - rj.w;
                        ri.x = rj.x - ri.w;
                    }
                }
                new_rects[j] = rj;
            }
            new_rects[i] = ri;
        }
    }
    {// Update the collision boxes.
        for (int i = 0; i < ccontext.cboxes_amount; ++i) {
            cboxes_at(i).rect = new_rects[i];
        }
    }
}

int physics_create_cbox(CollisionBoxInfo info)
{   log_calltrace();
    if (ccontext.cboxes_amount >= CBOX_MAX)
        return -1;
    int idx = ccontext.next_idx_queue[ccontext.cboxes_amount++];
    cboxes_at(idx).is_alive = true;
    cboxes_at(idx).rect = info.rect;
    cboxes_at(idx).group = info.group;
    cboxes_at(idx).last_hit = 0;
    cboxes_at(idx).fvector = (Point) {};
    return idx;
}

void physics_cbox_clear(int *box_id_ref)
{   log_calltrace(); assert(box_id_ref);
    cboxes_at(*box_id_ref).is_alive = false;
    ccontext.next_idx_queue[--ccontext.cboxes_amount] = *box_id_ref;
    *box_id_ref = -1;
}

Rect physics_get_cbox(int box_id)
{   log_calltrace();
    Rect result = cboxes_at(box_id).rect;
    if (cboxes_at(box_id).is_flipped)
        result.w = -result.w;
    return result;
}

void physics_update_cbox_dim(int box_id, int w, int h)
{   log_calltrace();
    if (cboxes_at(box_id).is_flipped) {
        Rect aux = cboxes_at(box_id).rect;
        cboxes_at(box_id).rect.x = aux.x + aux.w - w;
    }
    cboxes_at(box_id).rect.w = w;
    cboxes_at(box_id).rect.h = h;
}

void physics_cbox_add_vector(int box_id, Point vector)
{   log_calltrace();
    // TODO: only if the box is not falling.
    cboxes_at(box_id).fvector.x = vector.x;
    cboxes_at(box_id).fvector.y = vector.y;
}

void physics_cbox_clear_vector(int box_id)
{   log_calltrace();
    // TODO: only if the box is not falling.
    cboxes_at(box_id).fvector = (Point) {};
}

bool physics_grounded(int box_id)
{   log_calltrace();
    return cboxes_at(box_id).rect.y + cboxes_at(box_id).rect.h >= FLOOR_LEVEL;
}

void physics_flip_cbox(int box_id)
{   log_calltrace();
    cboxes_at(box_id).is_flipped = !cboxes_at(box_id).is_flipped;
}

bool physics_cbox_is_flipped(int box_id)
{   log_calltrace();
    return cboxes_at(box_id).is_flipped;
}

void physics_cbox_hit(int box_id)
{   log_calltrace(); assert(cboxes_at(box_id).is_alive);
    for (int i = 0; i < ccontext.cboxes_amount; ++i) {
        if (i != box_id &&
            SDL_GetTicks() - cboxes_at(i).last_hit > DAMAGE_DELAY &&
            rect_overlap(cboxes_at(box_id).rect, cboxes_at(i).rect)) {
            cboxes_at(i).got_hit = true;
            cboxes_at(i).last_hit = SDL_GetTicks();
            Point fvector = (Point) {.x = -3, .y = -2};
            if (cboxes_at(box_id).rect.x < cboxes_at(i).rect.x) {
                cboxes_at(i).is_flipped = true;
                fvector.x = -fvector.x;
            } else {
                cboxes_at(i).is_flipped = false;
            }
            physics_cbox_add_vector(i, fvector);
        }
    }
}

// TODO: Returns amount of damage taken.
bool physics_cbox_is_hit(int box_id)
{   log_calltrace();
    bool result = cboxes_at(box_id).got_hit;
    cboxes_at(box_id).got_hit = false;
    return result;
}

Rect physics_get_closest_enemy(int box_id)
{   log_calltrace();
    Rect rect = cboxes_at(box_id).rect;
    Rect result = (Rect) {};
    for (int i = 0; i < ccontext.cboxes_amount; ++i) {
        if (i != box_id && cboxes_at(i).group != GROUP_BACKGROUND &&
            cboxes_at(i).group != cboxes_at(box_id).group) {
            Rect rect_i = cboxes_at(i).rect;
            if (rect_equals(result, (Rect) {}) ||
                rect_dist(rect_i, rect) < rect_dist(result, rect)) {
                result = rect_i;
            }
        }
    }
    return result;
}
