#include "main.h"
#include "engine/loggers.h"
#include "engine/assets.h"
#include "engine/sprites_atlas.h"

// Implements.
#include "engine/sprite.h"

/***********************************
 *           ANIMATIONS            *
 ***********************************/

typedef struct animation *Animation;
struct animation {
    bool is_playing;
    unsigned first_tick; // When the current animation started.
    int delay;
    int aolines, cline, cframe;
    const Rect *frames[FAL_AMOUNT]; // Points to rects in sprites_atlas.
};

static void animation_init(Animation animation, enum which_sprite sprite_id)
{   log_calltrace();
    animation->aolines = FAL_AMOUNT;
    animation->cline = 0;
    for (int line = 0; line < FAL_AMOUNT; ++line) {
        animation->frames[line] = sprites_atlas[sprite_id][line];
    }
}

static int frame_count(Animation animation, int line)
{   log_calltrace();
    int count = 0;
    while (!rect_equals(animation->frames[line][count], NULL_RECT))
        ++count;
    return count;
}

/***********************************
 *            SPRITES              *
 ***********************************/
struct sprite {
    Texture texture;
    struct animation animation;
};

const size_t sprite_size = sizeof(struct sprite);

void sprite_init(Sprite sprite, enum which_sprite sprite_id)
{   log_calltrace();
    char *texture_fname = NULL;
    switch (sprite_id) {
    case SPRITE_OMEGA:
        texture_fname = OMEGA_PATH;
        break;
    case SPRITE_RYU:
        texture_fname = RYU_PATH;
        break;
    case SPRITE_SAGAT:
        texture_fname = SAGAT_PATH;
        break;
    default:
        fprintf(stderr, "[ERROR] invalid sprite_id: %d\n", sprite_id);
        exit(1);
    }
    assert(texture_fname);
    sprite->texture = video_load_texture(texture_fname);
    animation_init(&sprite->animation, sprite_id);
}

void sprite_destroy(SpriteRef sprite)
{   log_calltrace();
    video_destroy_texture(&(*sprite)->texture);
    *sprite = NULL;
}

Rect sprite_cframe(Sprite sprite)
{   log_calltrace();
    int cline = sprite->animation.cline;
    int cframe = sprite->animation.cframe;
    return sprite->animation.frames[cline][cframe];
}

void sprite_start_animation(Sprite sprite, int animation_line, int duration)
{   log_calltrace();
    assert(0 <= animation_line &&  animation_line < sprite->animation.aolines);
    sprite->animation.cline = animation_line;
    sprite->animation.is_playing = true;
    int aoframes = frame_count(&sprite->animation, animation_line);
    sprite->animation.first_tick = SDL_GetTicks();
    sprite->animation.delay = duration / aoframes;
}

void sprite_update_frame(Sprite sprite)
{   log_calltrace();
    // TODO: clean this.
    Animation animation = &sprite->animation;
    int cline = animation->cline;
    int et = SDL_GetTicks() - animation->first_tick;
    int cframe = et / animation->delay;

    animation->is_playing = !rect_equals(animation->frames[cline][cframe], NULL_RECT);
    if (animation->is_playing)
        animation->cframe = cframe;
}

bool sprite_draw(Sprite sprite, Rect box)
{   log_calltrace();
    int cline = sprite->animation.cline;
    int cframe = sprite->animation.cframe;
    Rect src = sprite->animation.frames[cline][cframe];
    //Rect dst = {.x = box.x, .y = box.y, .w = src.w * 3, .h = src.h * 3};
    video_draw_texture(sprite->texture, &src, &box);
    return sprite->animation.is_playing;
}
