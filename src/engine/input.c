#include <SDL2/SDL.h>
#include <SDL2/SDL_gamecontroller.h>
#include "main.h"
#include "engine/loggers.h"

// Implements.
#include "engine/input.h"

struct input g_input;

#define JOYSTICK_DEAD_ZONE 8000

static SDL_GameController *g_controller = NULL;

static void input_deinit(void)
{   log_calltrace();
    SDL_GameControllerClose(g_controller);
    SDL_QuitSubSystem(SDL_INIT_GAMECONTROLLER);
}

void input_init(void)
{   log_calltrace(); atexit(input_deinit);
    SDL_InitSubSystem(SDL_INIT_GAMECONTROLLER);
    SDL_ShowCursor(SDL_DISABLE);
}

void input_reset(void)
{   log_calltrace();
    g_input = (struct input) {};
}

void input_get(void)
{   log_calltrace();
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
	if (event.type == SDL_QUIT)
	    exit(0);
        if (event.type == SDL_CONTROLLERDEVICEADDED) {
            input_connect_controller();
        }
        if (event.type == SDL_CONTROLLERDEVICEREMOVED) {
            input_disconnect_controller();
        }
        if (event.type == SDL_KEYDOWN ||
            event.type == SDL_KEYUP) {
            bool down = event.type == SDL_KEYDOWN;
            switch (event.key.keysym.sym) {
            case SDLK_ESCAPE:
                g_input.esc = down; break;
            case SDLK_UP:
                g_input.up = down; break;
            case SDLK_DOWN:
                g_input.down = down; break;
            case SDLK_LEFT:
                g_input.left = down; break;
            case SDLK_RIGHT:
                g_input.right = down; break;
            case SDLK_RETURN:
                g_input.confirm = down; break;
            case SDLK_j:
                g_input.confirm = down; break;
            case SDLK_k:
                g_input.cancel = down; break;
            case SDLK_TAB:
                g_input.turn = down; break;
	    }
        } else if (event.type == SDL_CONTROLLERBUTTONDOWN ||
                   event.type == SDL_CONTROLLERBUTTONUP ||
                   event.type == SDL_CONTROLLERAXISMOTION) {
            bool down = ((event.type == SDL_CONTROLLERBUTTONDOWN) ||
                         (event.type == SDL_CONTROLLERAXISMOTION));
            switch (event.cbutton.button) {
            case SDL_CONTROLLER_BUTTON_BACK:
                g_input.esc = down; break;
            case SDL_CONTROLLER_BUTTON_DPAD_UP:
                g_input.up = down; break;
            case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
                g_input.down = down; break;
            case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
                g_input.left = down; break;
            case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
                g_input.right = down; break;
            case SDL_CONTROLLER_BUTTON_A:
                g_input.confirm = down; break;
            case SDL_CONTROLLER_BUTTON_B:
                g_input.cancel = down; break;
            case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
                g_input.turn = down; break;
            }
        }
    }
}

void input_connect_controller(void)
{   log_calltrace();
    for (int i = 0; i < SDL_NumJoysticks(); ++i) {
        if (SDL_IsGameController(i)) {
            g_controller = SDL_GameControllerOpen(i);
            if (g_controller) {
                return;
            } else {
                fprintf(stderr, "[ERROR] SDL_GameControllerOpen(%d): %s\n",
                        i, SDL_GetError());
            }
        }
    }
}

void input_disconnect_controller(void)
{   log_calltrace();
    assert(g_controller);
    SDL_GameControllerClose(g_controller);
    g_controller = NULL;
}
