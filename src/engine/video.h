#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "main.h"
#include "engine/assets.h"

typedef SDL_Color Color;
#define COLOR_BLACK (Color) {0, 0, 0, 255}
#define COLOR_WHITE (Color) {255, 255, 255, 255}
#define COLOR_RED (Color) {255, 0, 0, 255}
#define COLOR_CURSOR (Color) {196, 64, 64, 255}

typedef SDL_Rect Rect;
#define NULL_RECT (Rect) {}

extern const Rect video_res;

typedef SDL_Texture *Texture;
#define ATexture Texture __attribute__((cleanup(video_destroy_texture)))

typedef SDL_Point Point;
#define DNORTH (Point){.x = 0, .y = -1}
#define DSOUTH (Point){.x = 0, .y = 1}
#define DWEST (Point){.x = -1, .y = 0}
#define DEAST (Point){.x = 1, .y = 0}

void video_init(void);
void video_clear(void);
void video_present(void);
void video_draw_rect(Rect *dst, Color);
void video_draw_texture(Texture, Rect *src, Rect *dst);
Texture video_load_texture(const char *fname);
Texture video_load_texture_from_text(const char *text, SDL_Color color);
void video_destroy_texture(Texture *texture);

bool rect_equals(Rect a, Rect b);
int rect_dist(Rect a, Rect b);

bool point_equals(Point a, Point b);
int point_dist(Point a, Point b);
