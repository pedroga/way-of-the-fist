#pragma once

#include "main.h"
#include "engine/video.h"
#include "engine/sprites_atlas.h"

extern const size_t sprite_size;
typedef struct sprite *Sprite;
typedef Sprite *SpriteRef;

void sprite_init(Sprite, enum which_sprite);

inline __attribute__((always_inline))
Sprite sprite_create(enum which_sprite sprite_id) {
    Sprite new_sprite = alloca(sprite_size);
    sprite_init(new_sprite, sprite_id);
    return new_sprite;
}

void sprite_destroy(SpriteRef);

Rect sprite_cframe(Sprite);
void sprite_update_frame(Sprite);
bool sprite_draw(Sprite, Rect cbox);
void sprite_start_animation(Sprite, int animation_line, int duration);
