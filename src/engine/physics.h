#pragma once

#include "engine/video.h"

enum {
    GROUP_BACKGROUND, GROUP_PLAYER, GROUP_ENEMY
};

typedef struct {
    int group; // Boxes of the same group can overlap.
    Rect rect; // The position/dimensions of the box.
} CollisionBoxInfo;

//#define AFighter Fighter __attribute__((cleanup(fighter_destroy)))

/* The Physics System
 * works like a stack of contexts. Call physics_push() at the init of a mode,
 * then all physics operations will refer to that context; then, at the mode
 * deinit, call physics_pop(), so that when returning to the previous mode,
 * the physics system returs to where it was.
 * Before every redraw, call physics_tick().
 */
void physics_push(bool gravity);
void physics_pop(void);
void physics_update(void);

// Returns box_id to refer to the cbox afterwards.
int physics_create_cbox(CollisionBoxInfo info);
void physics_cbox_clear(int *box_id);
void physics_update_cbox_dim(int box_id, int w, int h);

void physics_cbox_add_vector(int box_id, Point vector);
void physics_cbox_clear_vector(int box_id);

Rect physics_get_cbox(int box_id);
bool physics_grounded(int box_id);

void physics_flip_cbox(int box_id);
bool physics_cbox_is_flipped(int box_id);

void physics_cbox_hit(int box_id);
bool physics_cbox_is_hit(int box_id);

Rect physics_get_closest_enemy(int box_id);
