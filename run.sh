#! /bin/bash

source ./make.sh

if [[ -v BUILD_FAILED ]]; then
    echo "Build failed. Not running the game."
else
    echo "Build ok... runing Way Of The Fist..."
    ./build/wotf.out
fi
