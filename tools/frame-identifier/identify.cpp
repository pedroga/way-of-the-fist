#include <algorithm>
#include <cstdio>
#include <utility>
#include <vector>
#include <deque>
#include <set>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>

using namespace std;

struct rect {
    int x, y, w, h;
};

char g_sprite_name[255] = "";
// pair<int, int> p -> p.first = line, p.second = column.
set<pair<int, int>> g_points = {};
vector<vector<pair<int, int>>> g_frames = {};
vector<rect> g_frame_rects = {};
vector<vector<rect>> result = {};

// Comparison functions for std::min_element and std::max_element.
bool pair_cmp_x(pair<int, int> i, pair<int, int> j)
{
    return i.second < j.second;
}
bool pair_cmp_y(pair<int, int> i, pair<int, int> j)
{
    return i.first < j.first;
}
// Comparison function for sorting rect vector.
bool rect_cmp_x(rect i, rect j)
{
    return i.x < j.x;
}


// Flood-fill.
void process(cv::Mat &m)
{
    { /* Prepare the set of non black points. */
        for (int i = 0; i < m.rows; ++i)
            for (int j = 0; j < m.cols; ++j )
                if (m.at<uchar>(i,j))
                    g_points.insert(pair<int,int>(i,j));
    }

    { /* Flood-fill: clear g_points, and add a vector of points (pertaining
       * to the same frame) to g_frames. */
        int cframe = 0;
        for (auto it = g_points.begin(); !g_points.empty(); it = g_points.begin(), ++cframe) {
            deque<pair<int, int>> dq;
            dq.push_back(*it);
            g_points.erase(it);

            g_frames.push_back({});
            while (!dq.empty()) {
                auto p = dq.front(); dq.pop_front();
                g_frames[cframe].push_back(p);

                auto left = pair<int, int>(p.first, p.second - 1);
                auto right = pair<int, int>(p.first, p.second + 1);
                auto up = pair<int, int>(p.first - 1, p.second);
                auto down = pair<int, int>(p.first + 1, p.second);

                auto left_up = pair<int, int>(p.first - 1, p.second - 1);
                auto right_up = pair<int, int>(p.first - 1, p.second + 1);
                auto left_down = pair<int, int>(p.first + 1, p.second - 1);
                auto right_down = pair<int, int>(p.first + 1, p.second + 1);

                if (g_points.find(left) != g_points.end()) {
                    dq.push_back(left);
                    g_points.erase(left);
                }
                if (g_points.find(right) != g_points.end()) {
                    dq.push_back(right);
                    g_points.erase(right);
                }
                if (g_points.find(up) != g_points.end()) {
                    dq.push_back(up);
                    g_points.erase(up);
                }
                if (g_points.find(down) != g_points.end()) {
                    dq.push_back(down);
                    g_points.erase(down);
                }
                if (g_points.find(left_up) != g_points.end()) {
                    dq.push_back(left_up);
                    g_points.erase(left_up);
                }
                if (g_points.find(right_up) != g_points.end()) {
                    dq.push_back(right_up);
                    g_points.erase(right_up);
                }
                if (g_points.find(left_down) != g_points.end()) {
                    dq.push_back(left_down);
                    g_points.erase(left_down);
                }
                if (g_points.find(right_down) != g_points.end()) {
                    dq.push_back(right_down);
                    g_points.erase(right_down);
                }
            }
        }
    }
    { /* Find the smallest rectangle that encapsulates all the points in each frame. */
        for (auto v : g_frames) {
            auto min_x = min_element(v.begin(), v.end(), pair_cmp_x);
            auto max_x = max_element(v.begin(), v.end(), pair_cmp_x);
            auto min_y = min_element(v.begin(), v.end(), pair_cmp_y);
            auto max_y = max_element(v.begin(), v.end(), pair_cmp_y);

            g_frame_rects.push_back(rect {.x = min_x->second, .y = min_y->first,
                                          .w = max_x->second - min_x->second + 1,
                                          .h = max_y->first - min_y->first + 1});
        }
    }
    { /* Sort the frame rects and find out to which animation line they belong. */
        int ty = g_frame_rects[0].y; // Current line top y.
        int by = ty + g_frame_rects[0].h; // Current line bottom y.
        int current_line = 0;
        result.push_back({});
        for (auto r : g_frame_rects) {
            if (ty <= r.y && r.y <= by) {
                result[current_line].push_back(r);
                if (by < r.y + r.h)
                    by = r.y + r.h;
            } else {
                result.push_back({});
                result[++current_line].push_back(r);
                ty = r.y; by = r.y + r.h;
            }
        }
        current_line = 0;
        printf("[%s] = {\n", g_sprite_name);
        for (auto line : result) {
            printf("    [%d] = {\n", current_line++);
            sort(line.begin(), line.end(), rect_cmp_x);
            for (auto r : line) {
                printf("        {.x = %d, .y = %d, .w = %d, .h = %d},\n",
                       r.x, r.y, r.w, r.h);
            }
            printf("    },\n");
        }
        printf("}, // End %s.\n", g_sprite_name);
    }
}

int main(int argc, char **argv)
{
    if (argc <= 1) {
        printf("Usage: %s <file-name> [- sprite_name]"
               "[file-name [- sprite_name]] ...\n", argv[0]);
        return 1;
    } else {
        for (int i = 1; i < argc; ++i) {
            {// Clear everything.
                g_sprite_name[0] = '\0';
                g_points.clear();
                g_frames.clear();
                g_frame_rects.clear();
                result.clear();
            }

            const char *fname = argv[i];
            if (argc > i + 2 && argv[i + 1][0] == '-') {
                strncpy(g_sprite_name, argv[i + 2], 254);
                i += 2;
            }
            cv::Mat img = imread(fname, cv::IMREAD_GRAYSCALE);
            if(img.empty()) {
                fprintf(stderr, "[ERROR] Could not read the image: %s\n", fname);
            } else {
                printf("//identifying: %s\n", fname);
                CV_Assert(img.depth() == CV_8U);
                process(img);
            }
        }
    }
    return 0;
}
