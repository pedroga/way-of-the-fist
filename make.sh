#! /bin/bash

# The compiler.
CC="gcc"

# Base path for headers.
BASE_HEADER_PATH="src/"

# Debug flags.
DFLAGS="-Wall -ggdb -DDEBUG"
# Release flags.
RFLAGS="-O2"

# Project's includes directory.
IDEPS="-I"${BASE_HEADER_PATH}
# Project's builds directory.
BDIR="./build"
# Binary name.
BIN="wotf.out"

#deps
SDL=" -lSDL2 -lSDL2_image -lSDL2_ttf"
LDEPS=${SDL}" -lm"

# Parse command line args.
for arg in "$@"
do
    if [ $arg == "--force" ]; then
        echo "Forcing compile everything"
        FORCE=1
    elif [ $arg == "--release" ]; then
        echo "Generating release binary"
        FORCE=1
        RELEASE=1
    elif [ $arg == "--clean" ]; then
        if [ -d "build" ]; then
            rm -r build
        fi
        exit 0
    elif [ $arg == "--rwindows" ]; then
        # Install mingw-w64 package: sudo apt install mingw-w64 (on Debian).
        CC="x86_64-w64-mingw32-gcc -DRWINDOWS"
        RELEASE=1
        IDEPS="${IDEPS} -ISDL2/include/"
        LDEPS="-L SDL2/bin -L SDL2/lib -lmingw32 -lSDL2main -mwindows ${LDEPS}"
        RFLAGS="${RFLAGS} ${LDEPS}"
        BIN="wotf.exe"
    fi
done

# Create build directory if it does not exist.
if [[ ! -d $BDIR ]]; then
    mkdir $BDIR
fi

function compile_release {
    local file=$1 # arg1 is the file being compiled.
    echo "[RELEASE] ${CC} ${RFLAGS} ${IDEPS} ${file} -c -o ${BDIR}/$(basename -s .c -- ${file}).o"
    $CC $RFLAGS $IDEPS $file -c -o $BDIR/$(basename -s .c -- $file).o
}

function compile_debug {
    local file=$1 # arg1 is the file being compiled.
    echo "[DEBUG] ${CC} ${DFLAGS} ${IDEPS} ${file} -c -o ${BDIR}/$(basename -s .c -- ${file}).o"
    $CC $DFLAGS $IDEPS $file -c -o $BDIR/$(basename -s .c -- $file).o
}

# This will only work if header have a full path starting at BASE_HEADER_PATH
# WARNING! Circular dependencies will break this.
function check_updated_file {
    local file=$1 # arg1 is the .c or .h file being checked.
    local object=$2 # arg2 is the .o file being generated.
    if [[ $file -nt $object ]]; then
        UPDATED_FILE=1
    else # Must look if the headers included in $file were updated.
        local included_headers=$(grep '#include "' $file)
        for header in $included_headers; do
            if [[ $header != "#include" ]]; then
                local header_path=$BASE_HEADER_PATH${header:1:-1} # Remove double quotes.
                if [[ ! ${checked_files[$header_path]} == 1 ]]; then
                    check_updated_file $header_path $object
                fi
                checked_files[$header_path]=1
            fi
        done
    fi
}

function build_objects {
    local dir=$1 # arg1 is the directory name.
    echo "Building .c files in "$dir

    for file in $(find $dir -type f -name '*.c'); do
        local object=$BDIR/$(basename -s .c -- $file).o
        unset UPDATED_FILE # Reset result of check_updated_file.
        if [[ ! -v FORCE ]]; then
            unset checked_files
            declare -A checked_files
            check_updated_file $file $object
        fi
        if [[ -v FORCE ]] || [[ -v UPDATED_FILE ]]; then
            UPDATED_SOME_OBJECT=1
            if [[ -v RELEASE ]]; then
                compile_release $file
            else
                compile_debug $file
            fi
            if [ $? -eq 1 ]; then
                BUILD_FAILED=1
            fi
        else
            echo "[SKIP] " $file " respective object file is newer."
        fi
    done
}

# Build src dir.
build_objects src

# Create executable if objects building was OK.
if [[ -v BUILD_FAILED ]]; then
    echo "Errors occurred while building."
elif [[ -v FORCE ]] || [[ -v UPDATED_SOME_OBJECT ]] || [[ ! -f $BDIR/$BIN ]]; then
    echo "BUILD OK... Linking..."
    echo "${CC} ${DFLAGS} ${LDEPS} ${BDIR}/*.o -o ${BDIR}/${BIN}"
    $CC $DFLAGS $LDEPS $BDIR/*.o -o $BDIR/$BIN
    if [ $? -eq 1 ]; then
        echo "LINKING FAILED"
        BUILD_FAILED=1
    else
        echo "CREATE EXECUTABLE: OK"
    fi
else
    echo "[SKIP] No object was updated, not generating new executable."
fi
