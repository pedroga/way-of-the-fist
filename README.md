# Way Of The Fist
This is a 2d fighting game based on street fighter. It combines with some rpg elements, where the player walks the world in an overworld view, and when a combat encounter occurs, the game enters a 2d fighting mode.
***
To compile this, cd to this directory and type:
- For linux -> ./make --release
- For windows -> ./make --rwindows

The SDL2 directory is included to make it easier to compile for windows, if you are on linux, you must install sdl2 sdl2_image and sdl2_ttf before compiling.
